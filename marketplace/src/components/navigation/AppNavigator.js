import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
//This component is used for give style to our navigator in this case the bottom tab navigator
import { TabBar } from 'react-native-animated-nav-tab-bar';
//This component like its name says is to render icons
import Icon from 'react-native-vector-icons/FontAwesome';
//This component will render the home screen in this case it would be the second bottom tab navigator option
import HomeScreen from '../../screens/HomeScreen';
//This component will render the settings screen in this case it would be the third bottom tab navigator option
import SettingsScreen from '../../screens/SettingsScreen';
//This component will render the explore screen in this case it would be the first bottom tab navigator option
import ExploreScreen from '../../screens/ExploreScreen';

const Tabs = createBottomTabNavigator();

//On tis component we render the BottomTabNavigator. For this app, this navigator has three options
const AppNavigator = () => {
  return (
    <Tabs.Navigator
      tabBarOptions={{
        activeTintColor: '#2F7C6E',
        inactiveTintColor: '#222222'
      }}
      tabBar={props => (
        <TabBar
          activeColors={'white'}
          activeTabBackgrounds={'#F9C815'}
          {...props}
        />
      )}
    >
      <Tabs.Screen
        name='Explore'
        component={ExploreScreen}
        options={{
          tabBarIcon: ({ focused, color, size }) => (
            <Icon
              name='compass'
              size={size ? size : 24}
              color={focused ? color : '#222222'}
              focused={focused}
            />
          )
        }}
      />
      <Tabs.Screen
        name='Home'
        component={HomeScreen}
        options={{
          tabBarIcon: ({ focused, color, size }) => (
            <Icon
              name='file'
              size={size ? size : 24}
              color={focused ? color : '#222222'}
              focused={focused}
            />
          )
        }}
      />
      <Tabs.Screen
        name='Settings'
        component={SettingsScreen}
        options={{
          tabBarIcon: ({ focused, color, size }) => (
            <Icon
              name='gear'
              size={size ? size : 24}
              color={focused ? color : '#222222'}
              focused={focused}
            />
          )
        }}
      />
    </Tabs.Navigator>
  );
};

module.exports = AppNavigator;
