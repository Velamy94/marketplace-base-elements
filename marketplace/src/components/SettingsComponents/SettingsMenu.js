import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import OptionMenu from './OptionMenu';
import { optionsMenu } from '../../services/data';

/*By means of map function and one array of objects we render the settings options, where
for each cicle we render one OptionMenu component and each OptionMenu component renders a
particular icon, option 's name and forward arrow. The OptionMenu component must receive
the title, icon color and icon name'*/
const SettingsMenu = () => {
  return (
    <View>
      {optionsMenu.map((item, index) => {
        return (
          <OptionMenu
            key={item.title}
            title={item.title}
            colorIcon={item.colorIcon}
            iconName={item.iconName}
            fontIcon={item.fontIcon}
          />
        );
      })}
    </View>
  );
};

module.exports = SettingsMenu;
