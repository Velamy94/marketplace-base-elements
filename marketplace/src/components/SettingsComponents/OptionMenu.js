import React, { useState } from 'react';
import { StyleSheet, TouchableOpacity, Text, View, Switch } from 'react-native';
//These variables are used to fetch the screen's width and height
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from 'react-native-responsive-screen';
import { Icon } from 'native-base';

/*On this component we render the 7 options of the settings menu.
In order to use correctly the switch component from react native we use the component useState
and a function to toogle a boolean value every time the user swipes the switch component*/
const OptionMenu = ({ title, iconName, colorIcon, fontIcon }) => {
  const [isEnabled, setIsEnabled] = useState(false);

  const toggleSwitch = () => {
    if (isEnabled) {
      setIsEnabled(false);
    } else {
      setIsEnabled(true);
    }
  };

  return (
    <View style={styles.container}>
      <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
        <View style={{ flexDirection: 'row' }}>
          <View
            style={{
              backgroundColor: colorIcon,
              width: 58,
              height: 51,
              borderRadius: 15,
              display: 'flex',
              justifyContent: 'center',
              marginRight: 15
            }}
          >
            <Icon
              type={fontIcon}
              name={iconName}
              style={{ alignSelf: 'center', color: 'white', fontSize: 26 }}
            />
          </View>
          <Text
            style={{
              alignSelf: 'center',
              color: 'rgb(99,95,95)',
              fontSize: 20,
              fontWeight: 'bold'
            }}
          >
            {title}
          </Text>
        </View>
        <View style={{ alignSelf: 'center' }}>
          {iconName === 'ios-moon' ? (
            <Switch
              trackColor={{ false: '#FFF', true: 'green' }}
              thumbColor={isEnabled ? 'white' : '#EEE'}
              ios_backgroundColor='#3e3e3e'
              onValueChange={toggleSwitch}
              value={isEnabled}
            />
          ) : (
            <TouchableOpacity>
              <Icon
                type='FontAwesome5'
                name='chevron-right'
                style={{
                  alignSelf: 'center',
                  color: 'rgb(119,110,110)',
                  fontSize: 16
                }}
              />
            </TouchableOpacity>
          )}
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: wp('90%'),
    alignSelf: 'center',
    height: 65,
    display: 'flex',
    justifyContent: 'center'
  }
});
module.exports = OptionMenu;
