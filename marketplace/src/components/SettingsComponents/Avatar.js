import React from 'react';
import { StyleSheet, Image, Text, View } from 'react-native';
import { avatar } from '../../services/data';

//On this component we render the user name and the user's photo

const Avatar = () => {
  return (
    <View style={styles.container}>
      <Image
        source={{
          uri: avatar.image
        }}
        style={styles.imgAvatar}
      />
      <View style={styles.labels}>
        <Text style={{ fontSize: 38 }}>{avatar.firstName}</Text>
        <Text style={{ fontSize: 38 }}>{avatar.lastName}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row'
  },
  imgAvatar: {
    width: 123,
    height: 123,
    marginLeft: 15,
    marginBottom: 48,
    marginTop: 48
  },
  labels: {
    flexDirection: 'column',
    display: 'flex',
    justifyContent: 'center',
    marginLeft: 15
  }
});

module.exports = Avatar;
