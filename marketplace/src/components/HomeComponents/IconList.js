import React from 'react';
import { FlatList, TouchableOpacity } from 'react-native';
import { Icon } from 'native-base';

/*This is a reusable component beacuse you can render any quantity of icons.
You need to provide two properties, a boolean value where this boolean property
will be useful to decide if you want to render the icons horizontally o vertically.
The other property is an object array. Each object should content the source where
the icon comes, the icon name, the icon color, the size as a number and finally as a number
as well how many space you want between every icon (marginRight)
*/

const IconList = ({ data, directionHorizontal }) => {
  return (
    <FlatList
      horizontal={directionHorizontal}
      data={data}
      keyExtractor={icon => icon.iconName}
      renderItem={({ item }) => {
        return (
          <TouchableOpacity onPress={() => {}}>
            <Icon
              type={item.fontIcon}
              name={item.iconName}
              style={{
                color: item.iconColor,
                fontSize: item.iconSize,
                marginRight: item.spaceRight
              }}
            />
          </TouchableOpacity>
        );
      }}
    />
  );
};

module.exports = IconList;
