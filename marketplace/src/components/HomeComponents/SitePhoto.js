import React from 'react';
import { Image } from 'react-native';

//This component renders an image, but you have to provide the width, height and the source

const SitePhoto = ({ w, h, src }) => {
  return (
    <Image
      style={{ width: w, height: h, alignSelf: 'center', marginLeft: 20, marginVertical: 5 }}
      source={{
        uri: src
      }}
    />
  );
};

module.exports = SitePhoto;
