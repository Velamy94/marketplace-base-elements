import React from 'react';
import { StyleSheet, View, Text } from 'react-native';

/*Like the name says, this component only render the header
in this case this component was use to render the HomeScreen header*/

const Header = () => {
  return (
    <View style={styles.container}>
      <Text style={styles.title}> My sites </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff'
  },
  title: {
    marginLeft: 15,
    marginTop: 30,
    marginBottom: 25,
    fontSize: 28,
    fontWeight: 'bold',
    color: 'rgb(99, 95, 95)'
  }
});

module.exports = Header;
