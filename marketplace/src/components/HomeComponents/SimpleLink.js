import React from 'react';
import { StyleSheet, TouchableOpacity, Text } from 'react-native';

//This is only a TouchableOpacity component rendered as a simple label

const SimpleLink = ({ message }) => {
  return (
    <TouchableOpacity onPress={() => {}}>
      <Text style={styles.title}> {message} </Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff'
  },
  title: {
    marginLeft: 15,
    marginTop: 30,
    marginBottom: 25,
    fontSize: 20,
    borderBottomWidth: 1,
    alignSelf: 'center',
    fontWeight: 'bold',
    color: 'rgb(99, 95, 95)'
  }
});

module.exports = SimpleLink;
