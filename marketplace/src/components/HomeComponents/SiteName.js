import React from 'react';
import { Text } from 'react-native';
//These variables are used to fetch the screen's width and height
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from 'react-native-responsive-screen';

/*This component only renders the name of the site. Here we use scrollview component in order to make
the component responsive and because is only text*/

const SiteName = ({ message, spaces, w }) => {
  return (
    <Text numberOfLines={2} ellipsizeMode='tail' style={{marginVertical: spaces,
    fontSize: 20,
    marginLeft: 15,
    width: wp(w),
    fontWeight: 'bold',
    color: 'rgb(99, 95, 95)'}}>
      {' '}
      {message}{' '}
    </Text>
  );
};

module.exports = SiteName;
