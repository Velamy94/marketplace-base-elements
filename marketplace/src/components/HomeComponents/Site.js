import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
//These variables are used to fetch the screen's width and height
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from 'react-native-responsive-screen';
/*These are necesary custom component to render the Site component
  SitePhoto component render a image from some site.
  SiteName component render a Text component with the site name,
  only you have to provide the site name as string value to SiteName component
  If you want to render some icons we use IconList
*/
import SitePhoto from './SitePhoto';
import SiteName from './SiteName';
import IconList from './IconList';

const Site = ({ name, img, dataIcon, description }) => {
  //Validations
  //If only the site's name is provided
  if (
    (img === null || img === undefined) &&
    (dataIcon === null || dataIcon === undefined) &&
    (description === null || description === undefined)
  ) {
    return (
      <View style={styles.container}>
        <View>
          <SiteName message={name} spaces={5} w='86.8%' />
        </View>
      </View>
    );
  } else if (
    (img === null || img === undefined) &&
    (dataIcon === null || dataIcon === undefined)
  ) {
    //If only the site`s name and description is provided
    return (
      <View style={styles.container}>
        <View>
          <SiteName message={name} spaces={5} w='86.8%' />
          <Text
            numberOfLines={2}
            ellipsizeMode='tail'
            style={{ width: wp('85%'), marginTop: 5, marginLeft: 15 }}
          >
            {' '}
            {description}
          </Text>
        </View>
      </View>
    );
  } else if (
    (dataIcon === null || dataIcon === undefined) &&
    (description === null || description === undefined)
  ) {
    //If only the site's name and the source is provided
    return (
      <View style={styles.container}>
        <View>
          <SitePhoto w={129} h={122} src={img} />
        </View>
        <View>
          <SiteName message={name} spaces={50} w='47%' />
        </View>
      </View>
    );
  } else if (dataIcon === null || dataIcon === undefined) {
    //If only the site's name, the source and description is provided
    return (
      <View style={styles.container}>
        <View>
          <SitePhoto w={129} h={122} src={img} />
        </View>
        <View>
          <SiteName message={name} spaces={20} w='47%' />
          <Text
            numberOfLines={2}
            ellipsizeMode='tail'
            style={{ width: wp('47%'), marginTop: 5, marginLeft: 15 }}
          >
            {' '}
            {description}
          </Text>
        </View>
      </View>
    );
  } else if (description === null || description === undefined) {
    //If there is no description
    return (
      <View style={styles.container}>
        <View>
          <SitePhoto w={129} h={122} src={img} />
        </View>
        <View style={styles.RightContainer}>
          <View style={{ flex: 3 }}>
            <SiteName message={name} spaces={30} w='47%' />
          </View>
          <View style={styles.actions}>
            <View style={{ top: 10 }}>
              <IconList data={dataIcon} directionHorizontal={true} />
            </View>
          </View>
        </View>
      </View>
    );
  } else if (img === null || img === undefined) {
    //If there is no image provided
    return (
      <View style={styles.container2}>
        <View>
          <SiteName message={name} spaces={5} w='86.8%' />
          <Text
            numberOfLines={2}
            ellipsizeMode='tail'
            style={{ width: wp('85%'), marginVertical: 5, marginLeft: 15 }}
          >
            {' '}
            {description}
          </Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            width: wp('87%')
          }}
        >
          <View style={{ top: 1 }}>
            <IconList data={dataIcon} directionHorizontal={true} />
          </View>
        </View>
      </View>
    );
  } else if (dataIcon !== null || dataIcon !== undefined) {
    //If there is no IconList information provided
    return (
      <View style={styles.container}>
        <View>
          <SiteName message={name} spaces={5} w='86.8%' />
        </View>
      </View>
    );
  } else {
    //If all the information is provided
    return (
      <View style={styles.container}>
        <View>
          <SitePhoto w={129} h={122} src={img} />
        </View>
        <View style={styles.RightContainer}>
          <View style={{ flex: 3 }}>
            <SiteName message={name} spaces={10} w='47%' />
            <Text
              numberOfLines={2}
              ellipsizeMode='tail'
              style={{ width: wp('47%'), marginTop: 5, marginLeft: 15 }}
            >
              {' '}
              {description}
            </Text>
          </View>
          <View style={styles.actions}>
            <View style={{ top: 10 }}>
              <IconList data={dataIcon} directionHorizontal={true} />
            </View>
          </View>
        </View>
      </View>
    );
  }
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flexDirection: 'row',
    alignSelf: 'center',
    width: wp('86.8%'),
    maxHeight: 136,
    borderRadius: 15,
    marginBottom: 25,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 5
    },
    shadowOpacity: 0.36,
    shadowRadius: 6.68,
    elevation: 5
  },
  container2: {
    backgroundColor: 'white',
    flexDirection: 'column',
    alignSelf: 'center',
    width: wp('86.8%'),
    maxHeight: 136,
    borderRadius: 15,
    marginBottom: 25,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 5
    },
    shadowOpacity: 0.36,
    shadowRadius: 6.68,
    elevation: 5
  },
  RightContainer: {
    flexDirection: 'column',
    justifyContent: 'flex-start',
    maxHeight: 136
  },
  actions: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    width: wp('50%'),
    bottom: 10
  }
});

module.exports = Site;
