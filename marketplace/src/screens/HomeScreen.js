import React from 'react';
import { StyleSheet, View, FlatList } from 'react-native';
//These variables are used to fetch the screen's width and height
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from 'react-native-responsive-screen';
/*Header, SimpleLink and Site are custom components
that they are going to render the homescreen elements on the screen.
The header component only render the title of the screen. The SimpleLink components
render other label that it can be pressed as a button. Site component is a custom
component that it contents an image (picture site), a label (The site's name) and three icons */
import Header from '../components/HomeComponents/Header';
import SimpleLink from '../components/HomeComponents/SimpleLink';
import Site from '../components/HomeComponents/Site';
/*mySites is an objects array.
This array has the information about the sites. This information will be used for site component*/
import { mySites, dataIcon } from '../services/data';

/*On this component we render the home screen elements but we provide the sites informations as well.
The information is rendered by means of the FlatList component.*/

const HomeScreen = () => {
  return (
    <View style={styles.container}>
      <View style={{ flexDirection: 'row' }}>
        <View style={{ flex: 1, justifyContent: 'flex-start' }}>
          <Header />
        </View>
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <SimpleLink message='Create new' />
        </View>
      </View>
      <FlatList
        data={mySites}
        keyExtractor={site => site.name}
        renderItem={({ item }) => {
          return (
            <View>
              <Site name={item.name} />
              <Site name={item.name} description={item.description} />
              <Site name={item.name} img={item.img} />
              <Site name={item.name} dataIcon={dataIcon} />
              <Site
                name={item.name}
                dataIcon={dataIcon}
                description={item.description}
              />
              <Site
                name={item.name}
                img={item.img}
                description={item.description}
              />
              <Site name={item.name} img={item.img} dataIcon={dataIcon} />
              <Site
                name={item.name}
                img={item.img}
                dataIcon={dataIcon}
                description={item.description}
              />
            </View>
          );
        }}
      />
      <View style={styles.tolerance} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: hp('91.5%'),
    top: hp('4.33%'),
    backgroundColor: 'white'
  },
  tolerance: {
    height: 30,
    backgroundColor: 'red',
    width: wp('86.8%')
  }
});

module.exports = HomeScreen;
