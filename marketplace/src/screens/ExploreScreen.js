import React from 'react';
import { StyleSheet, View, Text } from 'react-native';

//This component renders the Explore screen. It is the first option of the Bottom Tab Navigator

const ExploreScreen = () => {
  return (
    <View style={styles.container}>
      <Text> ExploreScreen </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  }
});

module.exports = ExploreScreen;
