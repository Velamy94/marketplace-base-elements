import React from 'react';
import { StyleSheet, View, Text, ScrollView } from 'react-native';
//These variables are used to fetch the screen's width and height
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from 'react-native-responsive-screen';
/*These are custom component. Avatar component contents the username and profile image.
SettingsMenu is a components that it renders the settings options*/
import SettingsMenu from '../components/SettingsComponents/SettingsMenu';
import Avatar from '../components/SettingsComponents/Avatar';

/*This component renders the Settings screen. It is the last option of the Bottom Tab Navigator.
Here we render the avatar information and and we render and 7 option inside a ScrollView component
*/

const SettingsScreen = () => {
  return (
    <View>
      <ScrollView style={styles.container}>
        <Avatar />
        <SettingsMenu />
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: hp('92%'),
    top: hp('4.33%'),
    backgroundColor: 'white'
  }
});

module.exports = SettingsScreen;
