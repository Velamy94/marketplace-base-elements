import React from 'react';
import { NavigationContainer } from '@react-navigation/native'; //This component is used for use navigations components
import AppNavigator from './src/components/navigation/AppNavigator';

/*This is the main app component, in this case has the navigation container for use navigation actions and
create navigators (BottomNavigator, TopNavigator, StackNavigator etc ...) The AppNavigator component contents a
BottomNavigator*/
export default function App() {
  return (
    <NavigationContainer>
      <AppNavigator />
    </NavigationContainer>
  );
}
